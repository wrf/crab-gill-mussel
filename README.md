# A study of mussels in gills of crabs #
...and a secondary mission of improving motivation and learning outcomes in lab practical courses.

## Background ##
The lab practical was a dissection course, each week covering a different animal group - e.g. squid, crabs, worms. Overall, I had the sense from talking to the students that many students enjoyed this course, for a few different reasons. They are all required to take the course, so some said they feel like it is a rite of passage of a biology student. Secondly, most lab courses are more popular than lectures, since students actually get to do something beyond just sitting. Thirdly, some students have specific interests in particular animal groups (also phobias).

During what was a normal week, the crab dissection, and student had pointed out that she had found a 1cm long shell in the gills of one of the crabs. Another professor said he had found that in previous years teaching this course as well, but we didn't know how common it really was. Only one reference had discussed this, and after contacting that author, we had decided to try and examine this systematically.

## Methods and data ##
Data were collected by SDU BB525 students during the zoology lab practicals, conducted every year at around the same week, beginning of December. Normally, the students would dissect the crab to identify anatomical features like gills, mouth parts, other organs if the condition was good. For the modified lab, the students additionally collected measurements on carapace width, sex (mostly male), color, whether any limbs were missing, any other signs of disease, the number of shells found in the gills, and the size of the shells.

### **I would like to give a special thanks to all the students of BB525 2019/2020/2021/2022 for help with data collection!** ###

This project was an ongoing collaboration with [Ben Ciotti](https://www.plymouth.ac.uk/staff/benjamin-ciotti) at Uni Plymouth- see paper by [Poulter 2018](https://doi.org/10.1007/s12526-016-0631-x) about the frequency of blue mussels in the gills of crabs.

![mussel_in_gill_20201202_171312-cr.jpg](https://bitbucket.org/wrf/crab-gill-mussel/raw/973012bfa7dd9d2c481d2eba7700ade787e85e77/images/mussel_in_gill_20201202_171312-cr.jpg)

In general, most of the crabs dissected in the course were males, and the ones with mussels in the gills tended to be larger. Similar observations were found with other sessile creatures in crab gills, such as the barnacle [*Octolasmis lowei*](https://marinespecies.org/aphia.php?p=taxdetails&id=106162) (formerly [*Octolasmis muelleri*](https://marinespecies.org/aphia.php?p=taxdetails&id=881957)) found by [Walker 1974](https://doi.org/10.2307/1540750). Again, because the inter-molting period becomes longer as the crabs grow, larger crabs are more likely to have mussels caught in the gills.

![2021_student_data.png](https://bitbucket.org/wrf/crab-gill-mussel/raw/18386e2e62941a93ab7986c7a264b565ede9d56d/images/2021_student_data.png)

## variation in crab color ##
examples are shown of typical "yellow" crabs vs a "red" crab. Point colors on the above graph are meant to roughly reflect this color. It is not known whether this has some effect on frequency of mussels in the gills.

![20191204_134537.jpg](https://bitbucket.org/wrf/crab-gill-mussel/raw/13681bddba81a4cb288420e87ce3b1864a42ef4c/images/20191204_134537.jpg){width=400px}

## variation in gill color ##
examples of unhealthy/diseased gills (black color, on left) vs normal/healthy gills (beige color, on right). It is not yet clear whether this is affected by the freezing.

![20191204_141849.jpg](https://bitbucket.org/wrf/crab-gill-mussel/raw/13681bddba81a4cb288420e87ce3b1864a42ef4c/images/20191204_141849.jpg){width=400px}

# some general considerations about courses and teaching #
I have encountered a range of attitudes by professors about teaching, from disinterest to pride. On one hand, some seem to teach since it is in the job description, and is required for them in order to keep their lab. On the other hand, I have known many professors that say things like "I am good at teaching" or "students like my class". We probably did not ask the students if they wanted to do anything other than listen to lectures, so it is hard to evaluate those statements outside the context of university. It could be that among half a dozen classes of entirely lectures per week, some are better than others, and students of course prefer those classes and those teachers. It also could be that given the choice of 20 hours of lectures per week, and 20 hours of doing the actual research, the students may choose the research, and skip the lectures entirely. I would reckon this is likely to be the case, but we do not know. 

Many non-scientist colleagues (or myself at a younger age) have the impression that most things are already known, and the rest of the work is sorting out frivolous details. It is easy to understand how people have this impression, since nearly all of school up to that point is spent memorizing stuff that other people already figured out. Usually there is very little pointed out about what IS NOT figured out, which would be an infinitely long and constantly changing list of topics. However, I think ALL of my scientist colleagues have told me that they felt that almost NOTHING is known. This difference in mentality is striking. To say it another way, some people feel that there is nothing left to discover, while some people (scientists) responsible for trying to discover new facts feel that there is a lot left to discover.

The students that I asked seemed to even prefer the crab task compared to the normal weeks, or at least not to mind the data collection. One student asked if we would be doing similar things for every week, like measuring the width of the squid or snails as well. No. We would not do that. It was just this one class, once per year.

Though it does not have to be that way. Courses could be changed to do actual science in real time. The two main problems are money, and restructuring what is allowed by curriculum committees if they already define what you are "supposed" to learn. 

Typically, lab courses have some budget already for consumables, so if a colleague can provide 100 samples that they already collected, then that is effectively "paid for" (from their own research budget). Some of the funding has to come from other research grants, like buying gene constructs for each gene for each student (say 100$ each). I have occasionally seen small, relatively uncompetitive grants for 1k$-10k$. I had even gotten one when I was a grad student.

The other problem is the general apathy of administration about doing anything, including modifying the curriculum or moving any money around. That is, the administration usually does not explicitly forbid such changes, but also does not help such initiatives, meaning you can do what you want, but do it on your own time and budget. Depending on the place, sometimes there is no rule about curriculum changes. Other times, I've heard about large meetings every 3 years. Somewhere in the middle might be to keep the task (like protein purification) but change the target.

That being said, I have several thoughts on how to restructure a lab course to do basic research:

### The result should matter to someone. ###
Someone should be trying to do something with the data and the results. This could be either the professor or the student themselves. For the professor, maybe there is not time to do all of the work themselves. The labs can be set up to do some of the work that the professor or a grad student would normally do. For most grad students, the results contribute to their thesis or a paper.

### There should be a consequence of a failed experiment. ###
The real world has consequences for doing your job right or wrong, like getting promoted or fired, yet this is rarely the case in most classes. The professor may not care whether a particular student answers a question correctly on a test, or passes. A student who does not do the experiment correctly probably has to just look up the result, or use another student's data for their lab report. However, even for a grad student, if the experiment does not work, then they have to do it again until it is done right, meaning there is a consequence to doing it wrong, i.e. more work for themselves.

### The task should be direct, and easily verified by the instructor. ###
I had one cheeky student ask how would I know if she did the procedure correct if there was not a known answer at the end. This is a fair point, but unlikely to occur without a conspiracy among the students that none of them do the experiment correctly. The instructor could also check any sample in real time, for instance if each student pair had a microscope. This can still apply to a multi-part project, like if over the course of the semester, students need to learn how to do several steps of a procedure, as long as each part can be verified along the way.

### The task should be scaleable and replicable. ###
Most scientists I know that do field work are overloaded with unanalyzed samples. There is a lot gained by having more replicates or more samples. It may be difficult for one researcher to process 100 samples, but very easy for a class of 50 students to process 2 each. The instructor should choose the most difficult ones for themselves to demonstrate how the procedure is done, or to avoid unfairly giving one student very difficult samples. There should be enough samples for each student to get at least one. Students should not work in groups, since this ALWAYS leads to laziness. Each student should collect their own data, and write their own report, even though the process is parallel between students. Additionally, this way it is very hard for anyone to copy someone else.

### Potential project ideas ###
With that in mind, here are some ideas of things that are direct, scaleable, and not known:

* Anything with VERY local relevance, local forests, trails, streams, or lakes.
* Data from those same streams, but analyzed over years, for water quality, species composition, color, sediment.
* Water samples from people's houses, though this should be done with caution since it must be done in a way where students are motivated to actually collect the samples for the class. For instance, if you are using a brand new ICPMS to check if there is lead or mercury in the water, then I suspect most students want to know if their water is safe. Otherwise it is likely that some students forget to collect the sample.
* Measurement of the protein or nutrient content of particular organs in some species of interest, like are microplastics detectable in all organs of a squid, other chemical pollutants, etc. In this sense, it might be too time consuming for one student to examine all organs, so from one whole animal, different organs can be distributed to different students. In parallel, they all have to do the homogenization, extraction, and measurement.
* Determining optimal growth conditions of some plant, or related, optimal conditions for production of some secondary metabolite of interest, like capsaicin from chili peppers, or toxins from some other plant. This both has to be grown, and then extracted and measured.
* Counting of cells under the microscope. This is kind of tedious after a few samples, so is very well suited to share with an entire class. These could be samples of living cells, like counts of different algae from a lake, cell morphology following some treatment, or something like [microfossil](https://en.wikipedia.org/wiki/Microfossil) samples. Because nearly all students have smartphones and cameras, it is easy to take a picture of an unidentified cell through the ocular and show that to the rest of the class on a projector.
* For a microbiology lab, investigation of growth inhibition of some bacterial species by another one (examples in a [review by Balouiri 2015](https://pmc.ncbi.nlm.nih.gov/articles/PMC5762448/)). An example was a study of *Lactobacillus* strains that inhibit coliform bacteria, by [Savino et al 2011](https://pmc.ncbi.nlm.nih.gov/articles/PMC3224137/). They investigated 27 strains, but this is by no means all the ones available, and could potentially add other ATCC/DSM strains, existing commercial probiotics, or identify new ones. 
* For a cell biology lab, examining karyotypes of local species. For instance, the common house mouse (in Europe) was found to have variable numbers of chromosomes due to fusions within and between populations (from [Bauchau et al 1990](https://doi.org/10.1111/j.1095-8312.1990.tb00829.x)). This could be examined for dogs, cats, squirrels, deer, fish, trees, etc. and is clearly a case where having more samples of the same species is very beneficial.
* For a cell biology lab, performing [in situ hybridizations](https://en.wikipedia.org/wiki/In_situ_hybridization) of all genes in a pathway in some species of interest at some developmental stage of interest. Many genomes of non-model organisms will contain genes unique to that group, or gene family expansions unique to that group. Each student can be given a different gene since it is relatively easy to buy constructs.
* For a biochemistry class, each student can be given a different gene from a large family, to generate transgenic cells, express the protein, purify it, and then characterize binding of reactants, cofactors, etc. For instance, this could be used to try and figure out the target motifs for a family of proteases, or variations in temperature sensitivity of the same protein (orthologs) across different species.
* For a biology class, basically anything with amplicon sequencing, since typically dozens or hundreds of samples need to be combined for a single run on the sequencer. The sample prep can be done by each student, and then pooled at the end.

Even for biologists who study a single protein, protein-protein interaction, structure, etc, there are still ways of scaling up. 

* Add another species, mouse, pig, cow, chicken (like ones that you can buy organic meat and organs from a butcher)
* Add mutants, say to change key amino acids in the structure, or interaction interface
* Check reactivity or binding to a variety of easy to acquire chemicals, like over-the-counter drugs.


